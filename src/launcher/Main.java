package launcher;

import Model.*;
import Vues.CapteurWindow;
import Vues.ImageWindow;
import Vues.MainWindow;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Main extends Application {
//--module-path ${JFX_PATH} --add-modules javafx.controls,javafx.fxml
    @Override
    public void start(Stage primaryStage) throws Exception {


        //Capteur pour tester les fenêtres
        Capteur capteurTest = new CapteurBasique(1, "capteur Test", new GenBoundedRandom());

        //Fenêtre images
        FXMLLoader loaderImage = new FXMLLoader(getClass().getResource("/ImageWindow.fxml"));
        ImageWindow imgW = new ImageWindow();

        //imgW.setCapteur(capteurTest);
        //capteurTest.attacherObs(imgW);

        loaderImage.setController(imgW);
        Parent image = loaderImage.load();
        Stage imageStage = new Stage();
        imageStage.setScene(new Scene(image));
        imageStage.setTitle("Fenêtre image");
        imageStage.show();

        //Fenêtre Capteur

        FXMLLoader loaderCapteur = new FXMLLoader(getClass().getResource("/CapteurWindow.fxml"));
        CapteurWindow captW = new CapteurWindow();

        //capteurTest.attacherObs(captW);

        loaderCapteur.setController(captW);
        Parent capteur = loaderCapteur.load();
        Stage capteurStage = new Stage();
        capteurStage.setScene(new Scene(capteur));
        capteurStage.setTitle("Fenêtre capteur");
        capteurStage.show();

        //Fenêtre principale
        FXMLLoader loaderMain = new FXMLLoader(getClass().getResource("/MainWindow.fxml"));
        MainWindow mainWindow = new MainWindow(imgW,captW);
        loaderMain.setController(mainWindow);
        Parent root = loaderMain.load();
        primaryStage.setScene(new Scene(root));
        primaryStage.setTitle("Fenêtre principale");
        primaryStage.setOnCloseRequest(e -> Platform.exit()); //Si la fenêtre principale est fermée, l'application se ferme
        primaryStage.show();

        //captW.setCapteur(capteurTest);
    }
}