package Vues;

import Model.Capteur;
import Model.Observer;
import javafx.fxml.FXML;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;


public class CapteurWindow implements Observer{

    SpinnerValueFactory<Double> valueFactory = new SpinnerValueFactory.DoubleSpinnerValueFactory(-273.15,60.00,0.00, 0.50);

    @FXML
    private Spinner<Double> spinner;

    private Capteur capteur;

    public CapteurWindow(){}

    //Permet de modifier le capteur observé et de bind la valeur du spinner avec la température du capteur
    public void setCapteur(Capteur capteur) {
        this.capteur = capteur;
        spinner.getValueFactory().valueProperty().bindBidirectional(this.capteur.temperatureProperty());
        capteur.attacherObs(this);
    }

    @FXML
    public void initialize(){
        this.spinner.setValueFactory(valueFactory);
    }

    //Permet de changer de capteur, on enlève le binding sur le capteur précédent, puis on set avec le nouveau capteur
    public void changeCapteur(Capteur newCapteur) {
        spinner.getValueFactory().valueProperty().unbindBidirectional(capteur.temperatureProperty());
        capteur.dettacherObs(this);
        setCapteur(newCapteur);
    }

    @Override
    public void update() {}
}
