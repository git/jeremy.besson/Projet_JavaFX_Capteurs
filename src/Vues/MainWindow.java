package Vues;

import Model.*;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableArray;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TreeView;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class MainWindow  {


    private ImageWindow imgW;

    private CapteurWindow captW;

    private boolean firstCaptor = true;

    private Capteur copie;

    @FXML
    private ListView<Capteur> lesCapteursAVoir = new ListView<>();

    private ObservableList<Capteur> lesCapteurs = FXCollections.observableArrayList();

    private ListProperty<Capteur> capteurs = new SimpleListProperty<>(lesCapteurs);



    public ListProperty<Capteur> get(){
        return capteurs;
    }

    public ObservableList<Capteur> getLesCapteurs(){
        return capteurs.get();
    }

    public MainWindow(ImageWindow imgW, CapteurWindow captW){
        this.imgW=imgW;
        this.captW=captW;
    }

    @FXML
    public void initialize(){
        capteurs.add(new CapteurBasique(1,"capteur random", new GenBoundedRandom()));
        capteurs.add(new CapteurBasique(2,"capteur manuel", new GenManual()));
        capteurs.add(new CapteurBasique(3,"capteur random réaliste", new GenBoundedRealist(-20.00,60.00,0.00)));

        lesCapteursAVoir.itemsProperty().bind(capteurs);

        lesCapteursAVoir.setOnMouseClicked(event ->{
            Capteur selectedCaptor = lesCapteursAVoir.getSelectionModel().getSelectedItem();
                if(selectedCaptor != null){
                    if(firstCaptor){
                        System.out.println("Le capteur " + selectedCaptor.getName() + " est selectionné\n");
                        imgW.setCapteur(selectedCaptor);
                        captW.setCapteur(selectedCaptor);
                        firstCaptor = false;
                    }else{
                        imgW.ChangeCapteur(selectedCaptor);
                        captW.changeCapteur(selectedCaptor);
                        System.out.println("Le capteur " + selectedCaptor.getName() + " est selectionné\n");
                    }
                }
        });
    }
}
