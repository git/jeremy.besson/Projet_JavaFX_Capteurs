package Vues;
import Model.Capteur;
import Model.Observer;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class ImageWindow implements Observer {
    @FXML
    private ImageView imageTemp;

    public void setImageTemp(String imagePath) {
        this.imageTemp.setImage(new Image(imagePath));
    }

    private Capteur capteur;

    public ImageWindow(){}

    public void setCapteur(Capteur capteur) {
        this.capteur = capteur;
        this.capteur.attacherObs(this);

    }

    public void ChangeCapteur(Capteur newCapteur) {
        capteur.dettacherObs(this);
        setCapteur(newCapteur);
    }

    @FXML
    public void initialize(){}

    //Change l'image affichée en fonction de la température du capteur observé
    @Override
    public void update() {
        if(capteur.getTemperature() > 22){
            setImageTemp("/images/sun.jpg");
            return;
        } else if (capteur.getTemperature() < 0 ) {
            setImageTemp("/images/ice.jpg");
            return;
        }
        setImageTemp("/images/cloud.jpg");
    }
}
