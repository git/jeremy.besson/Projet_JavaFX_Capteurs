package Model;

import java.util.ArrayList;
import java.util.List;

public abstract class Observable {

    private List<Observer> observeurs = new ArrayList<>();

    public Observable(){}


    public void attacherObs(Observer observer){
        observeurs.add(observer);
    }

    public void dettacherObs(Observer observer){
        observeurs.remove(observer);

    }

    public void notifier(){
        for (Observer b:observeurs) {
            b.update();
        }
    };
}
