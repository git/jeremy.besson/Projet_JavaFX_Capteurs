package Model;

import javafx.beans.property.*;
import java.util.ArrayList;
import java.util.List;

public abstract class Capteur extends Observable{
    private int id;
    private String name;

    private List<Observer> observers = new ArrayList<>();
    private ObjectProperty<Double> temperature = new SimpleObjectProperty<>(0.00);

    public ObjectProperty<Double> temperatureProperty() {
        return temperature;
    }

    public Double getTemperature(){
        return temperature.get();
    }

    //Modifie la température actuelle et notifie les observeurs
    public void setTemperature(Double temperature){
        this.temperature.set(temperature);
        notifier();
    }
    protected GenerationStrat generator;

    public Capteur(int id,String name){
        this.id=id;
        this.name=name;
    }

    public int getId(){
        return id;
    }

    public String getName(){
        return name;
    }

    public void setGenerator(GenerationStrat generator){
        this.generator=generator;
    }

    //génère une température grâce à la stratégie de génération utilisée, la génération se fait différemment en fonction du type de capteur
    public void genValue() {}

    public void add(Capteur captor, Double poids){}

    public String toString(){

        return getId() + " : " + getName() + " : " + getTemperature();
    }
}
