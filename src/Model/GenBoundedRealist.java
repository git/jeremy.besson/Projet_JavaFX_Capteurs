package Model;

import java.util.Random;
import java.util.random.RandomGenerator;

public class GenBoundedRealist implements GenerationStrat{
    private double min;
    private double max;
    private double generatedValue;

    private boolean boundBlocked = false;
    private Random random;
    private double lastValue;
    private boolean firstused = false;

    public GenBoundedRealist(double min, double max, double firstValue){
        if(min > max){  //Si la borne inférieure à une plus grande valeur que la borne supérieure, on les inverse
            double temp = min;
            min = max;
            max = temp;
        }

        //On définit le minimum possible à -50 °
        if(min < -50){
            min = -50;
        }
        this.min=min;

        //En réaliste, on ne va pas au dessus de 50°C
        if(max > 50){
            max = 50;
        }
        this.max=max;



        lastValue=firstValue;

        this.random = new Random();
    }

    @Override
    public double generate() {
        //Si on a déjà utilisé la première valeur, on génère une nouvelle valeur
        if(firstused){
            //On génère la nouvelle valeur
            generatedValue = random.nextDouble() * (max - min) + min;
            lastValue = lastValue + (random.nextDouble() * (max - min) + min);

            //On fait attention à ce que la valeur finale reste dans les bornes
            if(lastValue + generatedValue > max){
                boundBlocked = true; //On renseigne qu'on a bloqué la génération aux bornes
                lastValue = max;
            }

            if(lastValue + generatedValue < min){
                boundBlocked = true; //On renseigne qu'on a bloqué la génération aux bornes
                lastValue = min;
            }

            //Si la valeur s'est heurtée aux bornes, on réinitialise le booleen et retourne la valeur telle quelle
            if(boundBlocked){
                boundBlocked = false;
                return lastValue;
            }
            //Sinon, on retourne la nouvelle valeur
            return lastValue + generatedValue;

        }
            //Dans la cas on l'on avait pas encore passé la première valeur, on la renvoi et on précise que la première valeur a été utilisée
            firstused = true;
            return lastValue;
    }
}
