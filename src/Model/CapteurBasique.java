package Model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

public class CapteurBasique extends Capteur implements Runnable{

    //Thread du capteur
    public Thread runner;

    //Temps d'attente avant de générer une nouvelle valeur, par défaut, 2 secondes, soit 2000 millisecondes
    private ObjectProperty<Integer> waitTime = new SimpleObjectProperty<>(2000);

    public ObjectProperty<Integer> waitTimeProperty() {
        return waitTime;
    }
    public Integer getWaitTime(){
        return waitTime.get();
    }

    //Constructeur
    public CapteurBasique(int id, String name, GenerationStrat generator) {
        super(id, name);
        this.generator = generator;
        this.startThread();
    }

    //Démarre un nouveau thread
    public void startThread(){
        runner = new Thread(this);
        runner.start();
    }

    public void stopThread(){
        runner.interrupt();
    }

    //génère une température grâce à la stratégie de génération utilisée
    public void genValue(){
        //Si la génération est manuelle, on n'utilise pas de générateur, on garde donc la valeur courante
        if(generator instanceof GenManual){
            setTemperature(getTemperature());
        }else{
        //Sinon, on change la valeur on fonction de la stratégie utilisée
        setTemperature(generator.generate());
        }
    }

    //Permet d'ajouter un capteur. Cette fonctionnalité n'est utilisable que pour les capteurs de zone, c'est pourquoi elle retourne une exception pour un capteur basique
    public void add(Capteur captor, Double poids){
            throw new RuntimeException("Tentative d'ajout de capteur sur un capteur basique : non autorisé");
    }


    //Cette méthode est appelée automatiquement avec l'utilisation du thread
    @Override
    public void run(){
        genValue();    //On génère une valeur
        try {
            runner.sleep(this.getWaitTime()); //On attend le temps défini entre deux générations
            run();                 //On recommence
        } catch (InterruptedException e) {
           throw new RuntimeException(e);
        }
    }
}
