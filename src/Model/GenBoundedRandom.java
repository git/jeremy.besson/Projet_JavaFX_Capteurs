package Model;

import java.util.Random;
import java.util.random.RandomGenerator;

public class GenBoundedRandom implements GenerationStrat {

    private double min;
    private double max;
    private boolean boundDefined;

    private Random random;

    public GenBoundedRandom(double min,  double max){
        if(min > max){  //Si la borne inférieure à une plus grande valeur que la borne supérieure, on les inverse
            double temp = min;
            min = max;
            max = temp;
        }

        this.max=max;

        if(min < -273.15){
            min = - 273.15;
        }

        this.min=min;

        boundDefined = true;
        random = new Random();
    }

    public GenBoundedRandom(){
        boundDefined = false;
        random = new Random();
    }


    @Override
    public double generate() {
        if(boundDefined){
            return random.nextDouble() * (max - min) + min;
        }
            return random.nextDouble() * 333.15 - 273.15;
            //Pour calculer un double, on utilise la méthode nextDouble de Random
            // On a définit notre maximum à 60°C et le minimum possible comme étant le zéro absolu (- 273.15°C)
            // 60 - (- 273.15) = 333.15
    }
}
