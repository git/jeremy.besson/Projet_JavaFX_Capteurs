package Model;

public interface GenerationStrat {
    public double generate();
}
