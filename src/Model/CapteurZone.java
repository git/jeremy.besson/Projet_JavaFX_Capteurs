package Model;

import java.util.*;

public class CapteurZone extends Capteur implements Observer{

    //Liste référençant les capteurs observés
    private List<Capteur> capteurs = new ArrayList<>();

    //Liste référençant le poids de chaque capteur observé
    private List<Double> poids = new ArrayList<>();

    private double poidTotal = 0.0;

    //Constructeur
    public CapteurZone(int id, String name) {
        super(id, name);
    }

    //Permet d'ajouter un capteur à la zone
    public void add(Capteur captor, Double poids){
        capteurs.add(captor);
        this.poids.add(poids);
        poidTotal = poidTotal + poids;
    }

    //Retire un capteur de la zone
    public void remove(int index){
        capteurs.remove(index);
        poidTotal = poidTotal - poids.get(index);
        poids.remove(index);
    }


    //Recalcule la température du capteur, étant la moyenne des température des capteurs, en prenant en compte leur poids
    //Ce recalcul est appelé dès qu'un des capteurs change de température, car il notifie ses observateurs
    // en sachant que le capteur de zone est à la fois un capteur et un observateur
    @Override
    public void update() {
        Double sommeTemp = 0.0; //Somme des températures des capteurs
        for (int i = 0; i < capteurs.size(); i++) { //On parcourt chacun des capteurs
                sommeTemp = sommeTemp + (capteurs.get(i).getTemperature() * poids.get(i)); //On calcule la somme des températures
        }
        //On change la température du capteur en divisant le résultat par la somme des poids pour obtenir la moyenne
        setTemperature(sommeTemp/poidTotal);

    }
}
